<?php
    /*
     * Opgave 03_02
     * 
     * Metoden metoden removePersonFromArray skal kunne fjerne en person fra et indexeret array.
     * Brug den indbyggede metode array_slice().
     * Se kapitel - Array -> Extracting multiple values og afsnittet Slicing Array
     */

      class Person
    {
        function removePersonFromArray()
        {
            $people = array("Tom", "Dick", "Harriet", "Brenda", "Jo");
            list($second, $third) = array_slice($people, 1, 2);
            echo $second, $third;
        }
    }
    $person = new Person;
    $person->removePersonFromArray();
?>