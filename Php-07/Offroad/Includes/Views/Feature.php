<?php 
    /* Model: Feature */
    require_once('DB.php');
    class Feature{
        public $featureId;
        public $featureDescription;
        public $featureImage;
        public $featureVideo;
        function __construct($featureDescription, $featureImage, $featureVideo){
            $this->featureDescription = $featureDescription;
            $this->featureImage = $featureImage;
            $this->featureVideo = $featureVideo;
        }
        
        function createNewFeature(Feature $feature){
            //SQL insert statement
            global $database;
            //var_dump($feature);
            $database->insert("INSERT INTO offroad2017.Feature (FeatureDescription, FeatureImage, FeatureVideo) VALUES($feature->featureDescription, $feature->featureImage, $feature->featureVideo)");
        }
        
    }
?>