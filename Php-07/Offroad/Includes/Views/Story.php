<?php
        //require_once("DB.php");
        class Story
        {
            //Metode der returnere alle historier fra database
            function returnAllStoriesView()
            {
                //Lokale variabler
                //Variablerne bruges til forbindelsen DB serveren
                $servername = "localhost";
                $username = "root";
                $password = "";
                $dbname = "offroad2017";
                // Opret forbindelse
                $conn = new mysqli($servername, $username, $password, $dbname);
                // tjek om der er forbindelse
                if ($conn->connect_error) {
                    die("Forbindelsen fejlede : " . $conn->connect_error);
                }
                
                //En variable der holder på vores SQL DML forespørgsel (DML = Data Manipulation Language)
                $sql = "SELECT * FROM story";
                //Resultatet fra forespørgslen gemmes i $result variablen.
                $result = $conn->query($sql);
                $storyCount = $result->num_rows;
                //While løkke der returnerer alle historier fra arrayet.
                while($row = $result->fetch_assoc()) {
                    $storyCount--;
                    if ($storyCount % 2 == 0)
                    {
                        echo "<li class='timeline-inverted'>";
                    }else {
                        echo "<li>";
                    };
                    echo "<div class='timeline-image'>";
                    echo "<img class='img-circle img-responsive' src='../Resources/IMG/" . $row["StoryImg"] . "' alt=''>";
                    echo "</div>";
                    echo "<a href='storydetail.php?id=" . $row["StoryID"] . "'>";
                    echo "<div class='timeline-panel'>";
                    echo "<div class='timeline-heading'>";
                    echo "<h4>" . $row["StoryDate"] . "</h4>";
                    echo "<h4 class='subheading'>" . $row["StoryName"] . "</h4>";
                    echo "</div>";
                    echo "<div class='timeline-body'>";
                    echo "<p class='text-muted'>" . $row["StoryDescription"] . "</p>";
                    echo "</div>";
                    echo "</a>";
                    echo "</div>";
                    echo "</li>";
                }
            }
            //Denne metode forespørger på alle historierne i databasen
            function returnStoryDetailView($storyid){
                $q = "SELECT * FROM story WHERE StoryID = $storyid";
                $db = new MySqlDatabase();
                $result = $db->query($q);
                $value = mysqli_fetch_assoc($result); //Omskrive object til assocciativt array
                echo "<h2>" . $value["StoryName"] . "</h2>";
                echo "<figure>";
                echo "<img class='img-responsive img-centered figure-img img-fluid img-rounded' src='../resource/img/" . $value["StoryImg"] . "' alt=''>";
                echo "<figcaption class='figure-caption'>" . $value["StoryImgCaption"] . "</figcaption>";
                echo "</figure>";
                echo "<br>";
                echo "<p>" . $value["StoryDescription"] . "</p>";
            }
            
           
        }
?>