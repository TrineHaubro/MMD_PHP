<?php

    //Lokale variabler til brug længere nede
    //Variablerne bruges til forbindelse til DB serveren
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "educationdb";

    //Opret forbindelse
    $conn = new mysqli ($servername, $username, $password, $dbname);
    //tjek om der er forbindelse
    if ($conn->connect_error) {                                         //motode der beskriver fejlen med forbindelsen
        die("Forbindelse fejlede: " . $conn->connect_error);
    }

    //En variable der holder på vores SQL DML forespørgsel (DM = Data Manipulation Language)                           //LIKE gør det muligt for brugeren at søge på et ord og så vil databasen udskrive alle ord der minder om.
    $sql = "SELECT * FROM story "; //    

    //Resultatet fra forespørgslen gemmes i $result variablen
    $result = $conn->query($sql);

    //Udskriver data (hvis der er noget i $result)
    if ($result->num_rows > 0) {
            //output data fra hver række
            while($row = $result->fetch_assoc()) {
              echo "Navn: " . $row["StoryName"]. " - Beskrivelse: " . $row["StoryDescription"]. " " . $row["StoryData"]. " <img src='" . $row["StoryImg"] . "' /><br>"; 
            }
    } else {
        echo "Ingen resultater fra databasen";
    }

    //Lukker forbindelsen
    $conn->close();
?>