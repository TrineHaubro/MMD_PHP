<?php
    /* Dette dokument skal indeholde de dataelementer der indgår i html siden.
     * Følgende dataelementer skal som minimum være udpenslet i dette dokument.:
     * - email 
     * - phone
     * - preferences
     * - jobs
     * - competencies
     *
     * Det vil være naturligt at lade ovenstående elementer være 
     */

     $firstname = "Klaus";
     $lastname = "Nørregaard";
     $email = "kl@eadania.dk";
     $phone = 41770410;

     $preferences = array('Programmering','Organisation og udvikling','Undervisning og læring');

     $jobs = array('Fiat Herning' => 'Hjemmeside','Scholl sko' => 'Hjemmeside og webshop','Grameta' => 'Hjemmeside og webshop','N Graversen' => 'Hjemmeside og digitale strategier');

     
     $competencies = array('Undervisning',array('PHP','C#','HTML','CSS'), array('AngularJS','Bootstrap','Foundation'));

?>