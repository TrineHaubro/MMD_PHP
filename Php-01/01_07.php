<?php

    /*datatyper 
    * string eller tekststrengsværdier
    */

    $a = "Dette er en tekststreng<br>";
    echo $a;

    $a = 'Dette er en tekststreng<br>';
    echo $a;

    $name = "Trine";
    echo "Hej, $name";
    echo "<br>";
    echo "Hej, $name";

    $a = "<p>Dette er en tekststreng der indeholder afsnitselementer</p>";
    echo $a;

    date_default_timezone_set("Europe/Copenhagen");
    $a = "<div>Dato i dag er: " . date("d. m Y") . " og kl. er: " . date("h:i:s");
    echo $a;

    //Taste om to stenge er extension_loaded
    echo "<br>";
    echo "Er de to variabler ens?<br>";
    $a = "abc";
    $b = "abc";

    if($a == $b)
    {
        echo 'ja, $a og $b er ens <br>';
    }

    ?>