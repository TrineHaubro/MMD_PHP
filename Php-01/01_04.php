<?php
    //datatyper og variabler. Dette dokument indlejres i det efterfølgende

    //tekststreng (string)
    $firstName = "Trine";

    //tekststreng (string)
    $lastName = "Lavesen";

    //nummerisk heltal (integer)
    $age = "27";

    //boolean (sandt/falsk)
    $inRelationship = true;

    //tekststeng 
    $work = "Studerende";

    //tekststreng 
    $workPlace = "Erhversakadami Dania";

    //array (en række)
    $hobbies = ["Film og Serier", "Svømning", "Madlavning"]

    ?>