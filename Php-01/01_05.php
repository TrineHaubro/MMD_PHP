<!-- Indhenter information fra 01_04.php -->

<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>Mine informationer</title>
    </head>
    <body>

        <?php include '01_04.php';
        
        echo "Jeg hedder " . $firstName . " " . $lastName . " og jeg er " . $age . "  år gammel. <br>";
        
        if($inRelationship == true) {
            echo "Jeg er i et forhold.<br>";
        }else {
            echo "Jeg er single.<br>";
        }

        echo "Til dagligt er jeg " . $work . " på " . $workPlace . ".<br>";

        echo "I min fritid bruger jeg tiden på " . $hobbies[0] . " , " . $hobbies[1] . " og ". $hobbies[2] . "";
        ?>

    </body>
</html>