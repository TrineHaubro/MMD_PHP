<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>POST eksempel</title>
    </head>
    <body>
        <form action="handledate.php" method="get">
            <label for="storyname">Indtast navn</label>
            <input type="text" name="storyname">
            <label for="storydescription">Indtast beskrivelse</label>
            <input type="text" name="storydescription">
            <label for="storydate">Vælg dato</label>
            <input type="datetime" name="storydate">
            <input type="submit">
        </form>
    </body>
</html>